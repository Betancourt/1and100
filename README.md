# User Module (React Native)

## Sections

* [Getting Started](#getting-started)
* [Run App](#run-app)
* [Generate Build](#generate-build)


## Getting Started

### Installation

Install it once globally:

```sh
$ npm install -g react-native-cli
```

You'll need to have Node v8.1.X or later on your machine. We strongly recommend using npm v5.4.X.

Install dependencies library:

```sh
$ cd usermodule/
$ npm install
$ react-native link
```

## Run App

### Android (Device or simulator)

```sh
$ cd usermodule/
$ react-native run-android
```

## Generate Build

### For Android

Make sure signing key will be generate for this app on your local. (Do not commit that settings and files on Gitlab)

Want to generate ? Follow this link : https://facebook.github.io/react-native/docs/signed-apk-android.html

```sh
$ cd usermodule/
$ npm run build-android
```

After process is completed then you can get the android build in `android/app/build/outputs/apk/app-release.apk`.
Rename that file to `UserModule.apk`

Now Android build is ready to upload on https://betafamily.com/supersend
