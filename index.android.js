
/**
 * Initial UserModule for iOs
 *
 */

import { AppRegistry } from 'react-native';
import App from './src/App';

AppRegistry.registerComponent('UserModule', () =>  App);