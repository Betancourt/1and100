/**
 * @providesModule Loader
 */

import React, { Component } from 'react';
import { connect } from "react-redux";
import { View,Dimensions} from 'react-native';
import BusyIndicator from 'react-native-busy-indicator';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';
class Loader extends Component {


    componentWillReceiveProps({loader}) {
        if (loader == true) {
            loaderHandler.showLoader("Loading");
        }
    }

    render() {
        return (
            <View style={{
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width,
                flex:1,
                position:'absolute',
                right:0,
                zIndex:999
            }}>
                <BusyIndicator />
            </View>
        );
    }
}
const mapStateToProps = state => ({
    loader: state.loader ? state.loader : false
});

export default connect(mapStateToProps)(Loader);
