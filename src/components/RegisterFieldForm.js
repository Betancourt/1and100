import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { storeUser, emailRegex, numberRegex, getCountry, getState,getCity } from 'global';
import { reduxForm, change } from 'redux-form'
import  AppImages from 'AppImages';
import  ReduxField from 'ReduxField';
import RegisterStyle from 'RegisterStyle';
import { Row, Col, Label, Button, Text, Icon, Grid } from 'native-base';
import StyleConfig from 'StyleConfig';
import API from 'AppUtils';
import { Dropdown } from 'react-native-material-dropdown';

class RegisterFieldForm extends Component {

    constructor(props) {
        super(props);
        this.cityRef = this.updateRef.bind(this, 'city');
        this.statesRef = this.updateRef.bind(this, 'states');
        this.countryRef = this.updateRef.bind(this, 'country');
        this.state = {
            passwordHidden: true,
            rePasswordHidden: true,
            countryData:[],
            stateData:[],
            cityData:[],
            dobValidate: '',
            countryIndex: 0,
            statesIndex: 0,
            cityIndex: 0,
            countryId:0,
            statesId:0,
            cityId:0,
            country: "Select Country",
            states:'Select State',
            city:'Select City',
        };
    }

     async componentWillMount(){
         await this._getCountry();
    }

    componentWillReceiveProps(nextProps) {
        const {initialValues} = nextProps;
        if(initialValues != null) {
            const {country, states, city} = initialValues;
            if(country != 0) {
                this.setState({country, states, city});
                this.forceUpdate();
            }
        }
    }

    _handleSubmit = (formData) => {
        const { onSubmit } = this.props;
        const { countryId, statesId, cityId } = this.state;
        const formDataMix = Object.assign(formData,{'actived':1,'user_type_id':1,'customer_country_id':countryId, 'customer_state_id':statesId, 'customer_city_id':cityId, type:0});
        onSubmit(formDataMix);
    }

    _handleFormatChangeText = (fieldName, value) => {
        const { dispatch } = this.props;
        dispatch(change('registerForm', fieldName, value));
    }

    _getCountry = async() => {
        const { loader, toast } = this.props;
        loader(true);
       await API.getCountry().then((res) => {
            const { error, data } = res;
            if (error){
                const {response} = data;
                this.setState({'countryData':[]});
                let countries = [{id_country: 0, country_name: "Select Country"}];
                this.setState({'countryData':countries});
                loader(false);
                toast({text: response, type:'danger'});
            } else {
                let { countries } = data;
                this.setState({'countryData':[]});
                countries.unshift({id_country: 0, country_name: "Select Country"});
                this.setState({'countryData':countries});
                this.forceUpdate();
                loader(false);
            }
        });
    }

    _getState = async () => {
        const { loader, toast } = this.props;
        loader(true);
        const { countryIndex, statesId } = this.state;
        this.setState({countryId:this.state.countryData[countryIndex].id_country});
        if(countryIndex !== undefined && countryIndex !== null){
            API.getState({'id':this.state.countryData[countryIndex].id_country}).then((res) => {
                const { error, data } = res;
                if (error){
                    const { response } = data
                    this.setState({'statesData':[{id_state: 0, state_name: "Select State"}],'cityData':[{id_city: 0, city_name: "Select City"}]});
                    this.forceUpdate();
                    loader(false);
                    toast({text: response, type:'danger'});
                } else {
                    const { States } = data;
                    if(States.length > 0){
                        this.setState({'statesData':[],'cityData':[]});
                        States.unshift({id_state: 0, state_name: "Select State"});
                        this.setState({'statesData':States});
                        this.forceUpdate();
                    } else {
                        this.setState({'statesData':[{id_state: 0, state_name: "Select State"}],'cityData':[{id_city: 0, city_name: "Select City"}]});
                        this.forceUpdate();
                    }

                    this.forceUpdate();
                    loader(false);
                }
            });
        }

    }

    _getCity = async () => {
        const { loader, toast } = this.props;
        const { statesIndex, statesId } = this.state;
        this.setState({statesId:this.state.statesData[statesIndex].id_state});
        if(statesIndex !== undefined && statesIndex !== '' && statesIndex !== null){
            loader(true);
            API.getCity({'id':this.state.statesData[statesIndex].id_state}).then((res) => {
                const { error, data } = res;
                if (error){
                    const { response } = data;
                    this.setState({'cityData':[{id_city: 0, city_name: "Select City"}]});
                    loader(false);
                    toast({text: response, type:'danger'});
                } else {
                    const { Cities } = data;
                    if(Cities.length > 0){
                        Cities.unshift({id_city: 0, city_name: "Select City"});
                        this.setState({'cityData':Cities});
                        this.forceUpdate();
                    } else {
                        this.setState({'cityData':[]});
                        this.setState({'cityData':[{id_city: 0, city_name: "Select City"}]});
                    }
                    loader(false);
                }
            });
        }
    }

    updateRef(name, ref) {
        this[name] = ref;
    }

    onChangeText = async (text,index) => {
        let currentName = '';
        await ['name', 'city', 'states', 'country']
            .map((name) => ({ name, ref: this[name] }))
            .filter(({ ref }) => ref && ref.isFocused())
            .forEach(({ name, ref }) => {
                currentName = name;
                this.setState({ [name]: text ,[name+'Index']:index});
                if([name+'Index'] == 'statesIndex'){
                    this.setState({statesId:this.state.statesData[index].id_state});
                } else if([name+'Index'] == 'cityIndex'){
                    this.setState({cityId:this.state.cityData[index].id_city});
                } else if([name+'Index'] == 'countryIndex'){
                    this.setState({countryId:this.state.countryData[index].id_country});
                }
            });

            if(currentName !== '') {
                if(currentName == 'country') {
                    this.setState({'statesData':[],'cityData':[]});
                    this.setState({'statesData':[{id_state: 0, state_name: "Select State"}],'cityData':[{id_city: 0, city_name: "Select City"}]});
                    await this._getState();
                    this.forceUpdate();
                } else if (currentName == 'states') {
                    this.setState({'cityData':[]});
                    this.setState({'cityData':[{id_city: 0, city_name: "Select City"}]});
                    await this._getCity();
                    this.forceUpdate();
                }
            }
    }

    getItemLayout = (data, index) => {
      const length = StyleConfig.countPixelRatio(48)
      return {
        length,
        index,
        offset: length * index
      }
    }

    render(){
        const { handleSubmit, invalid, onSubmitFacebook } = this.props;
        const { passwordHidden, countryData, statesData, cityData } = this.state;
        let { country, states, city } = this.state;

        return(
             <Col style={[RegisterStyle.mainCol]}>
                <Row style={RegisterStyle.iconRow}>
                    <Col style={RegisterStyle.logoCol}>
                        <Text style={RegisterStyle.titleText}>
                            Sign Up
                        </Text>
                    </Col>
                </Row>
                 <Row style={RegisterStyle.footerIconRow}>
                    <TouchableOpacity onPress={()=>(onSubmitFacebook(this))}>
                        <Col style={RegisterStyle.footerIconColFB}>
                            <Image style={RegisterStyle.socialImages} source={AppImages.iconFacebook} />
                        </Col>
                    </TouchableOpacity>
                 </Row>
                <Row style={RegisterStyle.signinWithRow}>
                    <Col>
                        <Text style={RegisterStyle.signinWithTxt}>
                            or Sign Up with:
                        </Text>
                    </Col>
                </Row>
              <Row>
                    <Col>
                        <ReduxField
                            name="customer_name"
                            placeholder="Name"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            autoFocus={true}
                            showSideError={true}
                        />
                        <ReduxField
                            name="customer_lastname"
                            placeholder="Last Name"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                        <ReduxField
                            name="nickname"
                            placeholder="Nickname"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                         <ReduxField
                            name="customer_address"
                            placeholder="Address"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                        <ReduxField
                            name="customer_phone"
                            placeholder="Phone"
                            keyboardType='numeric'
                            maxLength={12}
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            placeholderStyle={RegisterStyle.placeholderMargin}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                       <ReduxField
                                name="customer_email"
                                placeholder="Email"
                                style={RegisterStyle.registerFormInput}
                                placeholderTextColor={StyleConfig.lightWhite}
                                placeholderStyle={RegisterStyle.placeholderMargin}
                                autoCapitalize="none"
                                autoCorrect={false}
                                showSideError={true}
                                />
                        <ReduxField
                            name="password"
                            placeholder="Password"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            placeholderStyle={RegisterStyle.placeholderMargin}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                            secureTextEntry={passwordHidden}
                           />
                        <ReduxField
                            name="confirm_password"
                            placeholder="Confirm Password"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            placeholderStyle={RegisterStyle.placeholderMargin}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                            secureTextEntry={passwordHidden}
                        />
                        <Dropdown
                            ref={this.countryRef}
                            value={country}
                            onChangeText={this.onChangeText}
                            data={countryData}
                            baseColor={StyleConfig.lightWhite}
                            textColor={StyleConfig.lightWhite}
                            selectedItemColor={StyleConfig.navyDark}
                            dropdownOffset={{top:0,left:0}}
                            style={[RegisterStyle.registerFormInput,RegisterStyle.placeholderMargin,{marginLeft: 15}]}
                            valueExtractor={({ country_name }) => country_name}
                        />
                        <Dropdown
                            ref={this.statesRef}
                            value={states}
                            onChangeText={this.onChangeText}
                            data={statesData}
                            baseColor={StyleConfig.lightWhite}
                            textColor={StyleConfig.lightWhite}
                            selectedItemColor={StyleConfig.navyDark}
                            dropdownOffset={{top:20,left:0}}
                            style={[RegisterStyle.registerFormInput,{marginLeft: 15}]}
                            valueExtractor={({ state_name }) => state_name}
                        />
                        <Dropdown
                            ref={this.cityRef}
                            value={city}
                            onChangeText={this.onChangeText}
                            data={cityData}
                            baseColor={StyleConfig.lightWhite}
                            textColor={StyleConfig.lightWhite}
                            selectedItemColor={StyleConfig.navyDark}
                            dropdownOffset={{top:20,left:0}}
                            style={[RegisterStyle.registerFormInput,{marginLeft: 15}]}
                            valueExtractor={({ city_name }) => city_name}
                        />
                        <ReduxField
                            name="card_number"
                            placeholder="Credit or debit card number"
                            style={[RegisterStyle.registerFormInput,{marginTop:StyleConfig.countPixelRatio(20)}]}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            keyboardType='numeric'
                            maxLength={16}
                            autoCorrect={false}
                            showSideError={true}
                        />
                        <ReduxField
                            name="expiration_date"
                            placeholder="Expiration Date"
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                        <ReduxField
                            name="cvv"
                            placeholder="CVV"
                            keyboardType='numeric'
                            maxLength={3}
                            style={RegisterStyle.registerFormInput}
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                        />
                    </Col>
                </Row>
                <Row style={RegisterStyle.signupButtonRow}>
                    <Col>
                        <TouchableOpacity onPress={handleSubmit(this._handleSubmit)}
                                          disabled={invalid}
                                          style={[RegisterStyle.signupButton,(invalid) ? RegisterStyle.btnDisabled : RegisterStyle.btnEnabled]}>
                            <Text style={[RegisterStyle.signupButtonText, (invalid) ? RegisterStyle.signupBtnTxtInValid : RegisterStyle.signupBtnTxtValid]}>Sign Up</Text>
                        </TouchableOpacity>
                    </Col>
                </Row>
            </Col>
        )
    }
}

const validate = values => {
    let errors = {};
    errors.customer_email = !values.customer_email
        ? 'Email Required'
        : !emailRegex.test(values.customer_email)
            ? 'Invalid'
            : undefined;

    errors.password = !values.password
        ? 'Required'
        : (values.password.length < 8)
            ? '8 character minimum'
            : undefined;

    errors.confirm_password = !values.confirm_password
        ? 'Required'
        : (values.confirm_password != values.password)
            ? 'Does not match'
            : undefined;
    errors.customer_name = !values.customer_name
        ? 'Required'
        : undefined;
    errors.nickname = !values.nickname
        ? 'Required'
        : undefined;

    errors.customer_lastname = !values.customer_lastname
        ? 'Required'
        : undefined;

    errors.customer_address = !values.customer_address
        ? 'Required'
        : undefined;

    errors.customer_phone = !values.customer_phone
        ? 'Required'
        : values.customer_phone.length < 10
            ? '10 digits minimum'
            : undefined;

    return errors;
};

const initialState = {
    customer_name:'',
    customer_lastname:'',
    customer_phone:'',
    customer_address:'',
    customer_email:'',
    nickname:'',
    password:'',
    confirm_password:'',
    country:'',
    states:'',
    city:'',
    nickname:'',
    actived:1,
    user_type_id:1,
    customer_country_id:1,
    customer_state_id:1,
    customer_city_id:1
};

const withForm = reduxForm({
    form: 'registerForm',
    initialValues: initialState,
    validate,
});

export default withForm(RegisterFieldForm);
