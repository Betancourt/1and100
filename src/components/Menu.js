/**
 * @providesModule Menu
 */

import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Content, Text, Thumbnail, Grid, Row, Col, Label } from 'native-base';
import { Actions } from 'react-native-router-flux';
import DrawerMenuStyle from 'DrawerMenuStyle';
import LayoutStyle from 'LayoutStyle';
import StyleConfig from 'StyleConfig';
import withUser from 'withUser';
import { userLogout, IMAGE_BASE_URL } from 'global';

class Menu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            updateAvailable: false,
        }
    }

    render(){
        const { updateAvailable } = this.state;
        return(
            <Content style={LayoutStyle.container} scrollEnabled={false}>
                <Grid>
                    <Row style={DrawerMenuStyle.gridRow}>
                        <Col style={DrawerMenuStyle.gridCol}>
                            <Row>
                                <Col></Col>
                                <Col style={DrawerMenuStyle.logoutCol}>
                                    <TouchableOpacity style={DrawerMenuStyle.logoutBtn} onPress={()=> userLogout()}>
                                        <Text style={DrawerMenuStyle.logoutBtnTxt}>LOG OUT</Text>
                                    </TouchableOpacity>
                                </Col>
                                <Col></Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </Content>
        )
    }
}

export default withUser(Menu);
