import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { Header, Body, Left, Right } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import withUser  from 'withUser';
import NavBarStyle from 'NavBarStyle';
import AppImages from 'AppImages';
import StyleConfig from 'StyleConfig';

class NavBar extends Component {

    _toggleDrawer = () => {
        if(Actions.currentScene == 'DrawerOpen') {
            Actions.drawerClose();
        } else {
            Actions.drawerOpen();
        }
    }

    render () {
        const { back, user } = this.props;
        return (
            <Header style={NavBarStyle.header}>
                <Left>
                    {
                        typeof back != 'undefined' && <TouchableOpacity onPress={() => Actions[back]()}>
                            <Icon name='chevron-left' style={NavBarStyle.backIcon} size={StyleConfig.headerIcon} color={StyleConfig.navyLight} />
                        </TouchableOpacity>
                    }
                </Left>
                <Body style={NavBarStyle.headerBody}>
                    <Image source={AppImages.bankLogo} style={NavBarStyle.logo} />
                </Body>
                <Right style={NavBarStyle.rightContainer}>
                    {
                        user != null && <TouchableOpacity onPress={() =>  this._toggleDrawer()}>
                            <Icon name='menu' size={StyleConfig.headerIcon} color={StyleConfig.navyLight} />
                        </TouchableOpacity>
                    }
                </Right>
            </Header>
        );
    }
}

export default withUser(NavBar);
