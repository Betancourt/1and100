import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { emailRegex } from 'global';
import { Label, Row, Col, Text } from 'native-base';
import { reduxForm } from 'redux-form';
import AppImages from 'AppImages';
import ReduxField from 'ReduxField';
import LoginStyle from 'LoginStyle';
import StyleConfig from 'StyleConfig';

class LoginFieldForm extends Component{

    constructor(props){
        super(props);
        this.state = {
            passwordHidden: true
        }
    }

    _changeView = () => {
        this.setState({
            passwordHidden: !this.state.passwordHidden
        });
    }

    render() {
        const { passwordHidden } = this.state;
        const { invalid, handleSubmit, onSubmit, onFacebookLogin } = this.props;
        return(
            <Col>
                <Row style={LoginStyle.inputRow}>
                    <Col>
                        <ReduxField
                            name="nickname"
                            placeholder="Nickname"
                            style={LoginStyle.input}
                            changeSuccessColor={true}
                            placeholderTextColor={StyleConfig.lightWhite}
                            placeholderStyle={LoginStyle.placeholderMargin}
                            autoCapitalize="none"
                            autoCorrect={false}
                            showSideError={true}
                            leftIcon={
                                <TouchableOpacity style={LoginStyle.inputHiddenBtn}>
                                    <Image source={AppImages.inputMail} style={LoginStyle.icon} />
                                </TouchableOpacity>
                            }
                            infoIcon={<Image source={AppImages.arrowSign} style={LoginStyle.icon1} />}
                        />

                        <ReduxField
                            name="password"
                            placeholder="Password"
                            style={LoginStyle.input}
                            leftIcon={
                                <TouchableOpacity style={LoginStyle.inputLeftBtn}>
                                    <Image source={ AppImages.inputPassword } style={LoginStyle.icon} />
                                </TouchableOpacity>
                            }
                            rightIcon={
                                <TouchableOpacity onPress={()=> this._changeView()} style={LoginStyle.inputHiddenBtn}>
                                    <Image source={(passwordHidden) ? AppImages.inputHidden : AppImages.inputVisible} style={LoginStyle.icon} />
                                </TouchableOpacity>
                            }
                            showSideError={true}
                            secureTextEntry={passwordHidden}
                            infoIcon={<Image source={AppImages.arrowSign} style={LoginStyle.icon1} />}
                            autoCapitalize="none"
                            placeholderTextColor={StyleConfig.lightWhite}
                            autoCorrect={false}
                            info="Forgot Password?"
                        />
                    </Col>
                </Row>
                <Row style={LoginStyle.loginRow}>
                    <Col>
                        <TouchableOpacity style={[LoginStyle.loginBtn,(invalid) ? LoginStyle.btnDisabled : LoginStyle.btnEnabled]}
                                disabled={invalid}
                                onPress={handleSubmit(onSubmit)}>
                            <Text style={[LoginStyle.loginBtnTxt, (invalid) ? LoginStyle.loginBtnTxtInValid : LoginStyle.loginBtnTxtValid]}>
                                Sign In
                            </Text>
                        </TouchableOpacity>
                    </Col>
                </Row>
            </Col>
        )
    }
}

const validate = values => {
    let errors = {};
    errors.nickname = !values.nickname
            ? 'Invalid'
            : undefined;

    errors.password = !values.password
        ? 'Password Required'
        : undefined;
    return errors;
};

const initialState = {
    nickname:'',
    password:''
};

const withForm = reduxForm({
    form: 'loginForm',
    validate,
    initialValues: initialState
});

export default withForm(LoginFieldForm);
