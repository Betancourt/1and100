import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import store from 'ReduxStore';
import MainContainer from 'MainContainer';
import Routes from "./Routes";
import LayoutStyle from 'LayoutStyle';
import { BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { getUser } from 'global';

class Register extends Component {
    constructor (props) {
        super(props);
    }

    componentDidMount () {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount () {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillReceiveProps(newProps) {
        Actions.refresh();
    }

    async onBackPress () {
        if (await getUser()) {
            Actions.main();
        } else {
            if (Actions.state.index === 0) {
                BackHandler.exitApp();
            }
            Actions.pop();
        }
        return true;
    }
    render () {
        return (
            <Provider store={ store }>
                <StyleProvider style={getTheme(material)}>
                    <View style={LayoutStyle.rootContainer}>
                        <MainContainer />
                        <Routes />
                    </View>
                </StyleProvider>
            </Provider>
        );
    }
}

export default Register;