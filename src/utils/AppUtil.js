/**
 * @providesModule AppUtils
 */

import { API_BASE_URL, getAuthToken, queryString, userLogout, APP_SECRET, APP_KEY } from 'global';


const isTokenExpire = async (responseJson) => {
    if (typeof responseJson == 'string' && responseJson.includes('token')) {
        userLogout();
        return false;
    }
    return responseJson;
}

const AppUtils = {
    facebookUser: async function (data) {
        return fetch(`https://graph.facebook.com/v2.10/${data.userId}?fields=name,email,picture&access_token=${data.token}`, {
            method: 'GET'
        }).then((response) => response.json());
    },

    login : async function(data) {
        return fetch(API_BASE_URL+'users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => {
            setTimeout(() => null, 0);
            return response.json();
        });
    },

    register : async function(data) {
        return fetch(API_BASE_URL+'users/signup', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => {
            setTimeout(() => null, 0);
            return response.json();
        });
    },

    getCountry : async function(data) {
        return fetch(API_BASE_URL+'country', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            setTimeout(() => null, 0);
            return response.json();
        });
    },

    getState : async function(data) {
        return fetch(API_BASE_URL+'state/'+data.id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            setTimeout(() => null, 0);
            return response.json();
        });
    },
    getCity : async function(data) {
        return fetch(API_BASE_URL+'city/'+data.id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            setTimeout(() => null, 0);
            return response.json();
        });
    },
}

module.exports = AppUtils;
