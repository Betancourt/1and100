/**
 * @providesModule NavBarStyle
 */

import { StyleSheet } from 'react-native';
import { WINDOW } from 'global';
import StyleConfig from 'StyleConfig';

export default StyleSheet.create({
    header: {
        paddingTop:StyleConfig.countPixelRatio(15),
        backgroundColor:StyleConfig.white,
        borderBottomColor: 'transparent',
        height: StyleConfig.countPixelRatio(WINDOW.height * 0.1),
        width: '100%',
    },
    headerBody:{
        alignItems:'flex-end',
    },
    headerLeftLogo:{
        resizeMode:'contain',
        alignItems:'center',
    },
    backIcon: {
        color:StyleConfig.navyLight,
        fontSize: StyleConfig.countPixelRatio(50),
        backgroundColor:'transparent'
    },
    logo:{
        width:StyleConfig.getWidthByColumn(4),
        resizeMode:'contain'
    },
    rightContainer: {
        paddingRight:WINDOW.width * 0.03,
    }
});