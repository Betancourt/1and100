/**
 * @providesModule DrawerMenuStyle
 */

import { StyleSheet } from 'react-native';
import StyleConfig from 'StyleConfig';

export default StyleSheet.create({
    gridRow:{
        backgroundColor:StyleConfig.navyDark,
    },
    gridCol: {
        width: StyleConfig.getWidthByColumn(1),
        backgroundColor:StyleConfig.navyDark,
        paddingRight:StyleConfig.getScreenPadding
    },
    thumbnailCol:{
        marginTop:StyleConfig.countPixelRatio(50),
        justifyContent:'center',
        alignItems:'center'
    },
    thumbnail:{
        width: StyleConfig.getWidthByColumn(2),
        height: StyleConfig.getWidthByColumn(2),
        borderRadius: StyleConfig.getWidthByColumn(2) / 2,
        resizeMode:'stretch'
    },
    nameCol:{
        marginTop:StyleConfig.countPixelRatio(15),
        justifyContent:'center',
        alignItems:'center'
    },
    nameLabel:{
        fontSize:StyleConfig.fontSizeH3,
        fontFamily:StyleConfig.gothamMedium,
        letterSpacing:1,
    },
    logoutCol:{
        marginVertical:StyleConfig.countPixelRatio(15),
        justifyContent:'center',
        alignItems:'center',
    },
    logoutBtnTxt:{
        color:StyleConfig.white,
        textAlign:'center',
        lineHeight: StyleConfig.buttonTextH1,
        fontSize:StyleConfig.buttonTextH1,
        fontFamily:StyleConfig.gothamMedium,
        letterSpacing:1,
    },
    logoutBtn:{
        width: StyleConfig.getWidthByColumn(2),
        borderColor:StyleConfig.white,
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        height: StyleConfig.buttonHeightH2,
        borderRadius:StyleConfig.buttonHeightH2,
    },
    menuMainCol: {
        alignItems:'flex-start',
        width:StyleConfig.getWidthByColumn(1),
        marginTop:StyleConfig.countPixelRatio(10),
    },
    menuCol:{
        justifyContent:'center',
        alignItems:'flex-start',
        paddingLeft:StyleConfig.getWidthByColumn(4) + StyleConfig.scalarSpace,
        height:StyleConfig.countPixelRatio(50),
    },
    menuTxt:{
        fontSize:StyleConfig.fontSizeH3,
        fontFamily:StyleConfig.gothamBold,
        letterSpacing:1,
    },
    menuFooter: {
        flex:1,
        justifyContent:'center',
        alignItems:'flex-start',
        height: StyleConfig.countPixelRatio(150),
        width:StyleConfig.getWidthByColumn(1),
        paddingLeft:StyleConfig.getWidthByColumn(4) + StyleConfig.scalarSpace,
    },
    versionText: {
        color:StyleConfig.navyMediumDark,
        fontFamily:StyleConfig.gothamBook,
        fontSize: StyleConfig.fontSizeH4,
        letterSpacing:1,
        height:StyleConfig.countPixelRatio(25)
    },
    updateBtn:{
        justifyContent:'center',
        alignItems:'center',
        height: StyleConfig.buttonHeightH2,
        borderRadius: StyleConfig.buttonHeightH2,
        backgroundColor: StyleConfig.blue,
        width: StyleConfig.getWidthByColumn(2)
    },
    updateBtnTxt:{
        textAlign:'center',
        lineHeight: StyleConfig.buttonHeightH2,
        backgroundColor:'transparent',
        color: StyleConfig.white,
        fontFamily: StyleConfig.gothamBold,
        fontSize: StyleConfig.buttonTextH1,
        letterSpacing:1
    },
    uptodateBtn:{
        justifyContent:'center',
        alignItems:'center',
        height: StyleConfig.buttonHeightH2,
        borderRadius:StyleConfig.buttonHeightH2,
        backgroundColor:StyleConfig.gray,
        width:StyleConfig.getWidthByColumn(2)
    },
    uptodateBtnTxt:{
        opacity:0.8,
        textAlign:'center',
        lineHeight: StyleConfig.buttonHeightH2,
        backgroundColor:'transparent',
        color:StyleConfig.white,
        fontFamily:StyleConfig.gothamBold,
        fontSize:StyleConfig.buttonTextH1,
        letterSpacing:1
    },
});
