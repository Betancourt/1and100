/**
 * @providesModule AppImages
 */

export default {
    iconFacebook: require('./icon-facebook.png'),
    inputHidden: require('./input-hidden.png'),
    inputVisible: require('./input-visible.png'),
    arrowSign: require('./icon-arrow.png'),
    inputMail: require('./icon-email.png'),
    inputPassword: require('./icon-lock.png'),
    arrowDropdown: require('./dropdown-arrow.png'),
}
