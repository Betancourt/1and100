import React, { Component } from 'react';
import { Content, Grid, Row, Col, Label, Button, Text,View } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { storeUser, emailRegex, numberRegex, FACEBOOK, SOCIAL_AUTH_CONFIG } from 'global';
import LayoutStyle from 'LayoutStyle';
import RegisterStyle from 'RegisterStyle';
import withToast from 'withToast';
import withLoader from 'withLoader';
import withRegisterUser from 'withRegisterUser';
import RegisterFieldForm from '../components/RegisterFieldForm';
import API from 'AppUtils';
import { facebook } from 'react-native-simple-auth';

class Register extends Component {

    constructor(props) {
        super(props);
    }

    _doFacebookSignup = async () => {
        const { loader, toast } = this.props;
        loader(true);
        await facebook(SOCIAL_AUTH_CONFIG.facebook)
            .then((data)=> {
                const {credentials:{access_token:access_token},user:{name,verified,id}} = data
                if (data != null) {
                    let registerUserData = {
                        customer_name: name.split(' ')[0],
                        customer_lastname: name.split(' ')[1],
                        type:1,
                        token:access_token,
                        customer_country_id:1,
                        customer_state_id:1,
                        customer_city_id:1,
                        user_type_id:1,
                        actived:true,
                        fb_id:id
                    };
                    API.register(registerUserData).then((res) => {
                        const { error, data } = res;
                        console.log("data---",data);
                        if (error){
                            loader(false);
                            const {response} = data;
                            toast({text: response, type:'danger'});
                            setTimeout(() => {
                                Actions.login();
                            }, 100);
                        } else {
                            loader(false);
                            toast({text:'You have register with facebook successfully', type:'success'});
                            setTimeout(() => {
                                Actions.login();
                            }, 100);
                        }
                    });
                } else {
                    loader(false);
                }
            });
    }

    _onSubmit = (formData) => {
        const { loader, toast } = this.props;
        setTimeout((formData) => { loader(true); }, 1);
        delete formData['confirm_password'];
        delete formData['cvv'];
        delete formData['expiration_date'];
        delete formData['card_number'];
        API.register(formData).then((res) => {
            const { error, data } = res;
            if (error){
                const {response} = data;
                toast({text: response, type:'danger'});
            } else {
                const { toast } = this.props;
                toast({text:'You have register successfully', type:'success'});
                Actions.login();
            }
        });
    }

    render () {
        const { registerUser } = this.props;
        const registerFieldForm = {
            initialValues:registerUser
        };
        return (
             <Content style={LayoutStyle.container}>
                <Grid style={LayoutStyle.mainGrid}>
                    <Row>
                        <Col>
                            <RegisterFieldForm {...this.props} onSubmitFacebook={this._doFacebookSignup.bind(this)} {...registerFieldForm} onSubmit={this._onSubmit.bind(this)} />
                        </Col>
                    </Row>
                      <Row style={RegisterStyle.signUpRow}>
                        <Col style={RegisterStyle.signUpTextCol}>
                            <Text style={RegisterStyle.signUpBtnLabel}>Already have an account ?
                                <Text style={RegisterStyle.signUpBtnTxt} onPress={() => Actions.login()}>
                                    &nbsp; Sign In
                                </Text>
                            </Text>
                        </Col>
                    </Row>
                </Grid>
            </Content>
        );
    }
}

export default withToast(withLoader(withRegisterUser(Register)));
