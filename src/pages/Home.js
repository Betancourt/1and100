/*import React, { Component } from 'react';
import { Content, Grid, Row, Text } from 'native-base';
import { storeUser, emailRegex, numberRegex } from 'global';
import LayoutStyle from 'LayoutStyle';
import RegisterStyle from 'RegisterStyle';
import withToast from 'withToast';
import withLoader from 'withLoader';
import withRegisterUser from 'withRegisterUser';
import withUser from 'withUser';

class Home extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
    }

    render () {
        return (
            <Content style={LayoutStyle.container}>
                <Grid style={LayoutStyle.mainGrid}>
                    <Row style={RegisterStyle.gridRow}>
                        <Text>Home</Text>
                    </Row>
                </Grid>
            </Content>
        );
    }
}

export default withUser(withToast(withLoader(withRegisterUser(Home)))); */



/* Codigo con el grafico*/

/**
 * Quick chance Registered trademark application
 * version : 1.0.0
 * Developer :Leonardo Betancourt Caicedo
 * Github : @leobetancourt92
 */
/*
import React, {Component, propTypes} from 'react';
import {
        Content,
        Grid,
        Text,
        Row
      } from 'native-base';

import withToast from 'withToast';
import withLoader from 'withLoader';
import withRegisterUser from 'withRegisterUser';
import withUser from 'withUser';
import {
        AppRegistry,
        StyleSheet,
        View,
        processColor,
        Alert,
        TouchableOpacity,
        StatusBar,
        Image,
        } from 'react-native';

import {StackNavigator, SafeAreaView} from 'react-navigation';

import {PieChart} from 'react-native-charts-wrapper';
//Incluimos la libreria react-native-awesome-alerts para manejo de alertas
import AwesomeAlert from 'react-native-awesome-alerts'; */

import React, {Component, propTypes} from 'react';
import {
AppRegistry,
        StyleSheet,
        Text,
        View,
        processColor,
        Alert,
        TouchableOpacity,
        StatusBar,
        Image,
        } from 'react-native';
  import withToast from 'withToast';
  import withLoader from 'withLoader';
  import withRegisterUser from 'withRegisterUser';
  import withUser from 'withUser';


import {StackNavigator, SafeAreaView} from 'react-navigation';

import {PieChart} from 'react-native-charts-wrapper';
//Incluimos la libreria react-native-awesome-alerts para manejo de alertas
import AwesomeAlert from 'react-native-awesome-alerts';


//Incluimos la libreria react-native-dropdownalert para manejo de ventanas emergentes
// desde la parte superior del dispositivo
//import DropdownAlert from 'react-native-dropdownalert';
/*
 *
  <DropdownAlert
                            ref={(ref) => this.dropdown = ref}
                            containerStyle={{
                                                backgroundColor: MAIN_CUSTOM_COLOR,
                            }}
                            showCancel={true}
                            onClose={(data) => this.onClose(data)}
                            onCancel={(data) => this.onClose(data)}
                            imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                            />
 *
 */

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {showAlert: false};
        this.state = {
            legend: {
                enabled: true,
                textSize: 8,
                form: 'CIRCLE',
                position: 'RIGHT_OF_CHART',
                wordWrapEnabled: true
            },
            data: {
                dataSets: [{
                        values: [{value: 10, label: '$20'},
                            {value: 10, label: '$100'},
                            {value: 10, label: '$200'},
                            {value: 10, label: '$300'},
                            {value: 10, label: '$400'}],
                        label: 'Jackpot list',
                        config: {
                            colors: [processColor('#C0FF8C'), processColor('#FFF78C'), processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D')],
                            valueTextSize: 20,
                            valueTextColor: processColor('green'),
                            sliceSpace: 5,
                            selectionShift: 13
                        }
                    }],
            },
            highlights: [{x: 2}],
            description: {
                text: 'Quick Chance!!!',
                textSize: 15,
                textColor: processColor('darkgray'),

            }
        };
    }

    /**
     * Declaramos el metodo ShowAlert para instanciar una alerta (hacerla visible)
     */

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
            /**
             *Declaramos el metodo hideAlert para ocultar la alerta
             */
            hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };
            //alertTitle = {this.state.selectedEntry + "hola"};

            /**
             * Por  medio de esta funcion  se genera un json con los valores de el area seleccionada (evento)
             */
            handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({...this.state, selectedEntry: null})
        } else {
            this.setState({...this.state, selectedEntry: JSON.stringify(entry.data.label)})

            //Agregamos un cuadro de confirmacion para la transaccion en caso de que la persona este apostando en el rango
            //100-400
            this.setState({showAlert: true});

        }
        console.log(event.nativeEvent)
        //event.nativeEvent.ShowAlert();
    }

    render() {
        const {showAlert} = this.state;
        return (
                <SafeAreaView style={{flex: 1}}>
                    <View>
                        <Text>selected:</Text>
                        <Text> {this.state.selectedEntry}</Text>
                    </View>

                    <View style={styles.container}>
                        <PieChart
                            style={styles.chart}
                            logEnabled={true}
                            chartBackgroundColor={processColor('pink')}
                            chartDescription={this.state.description}
                            data={this.state.data}
                            legend={this.state.legend}
                            highlights={this.state.highlights}

                            entryLabelColor={processColor('black')}
                            entryLabelTextSize={20}
                            drawEntryLabels={true}

                            rotationEnabled={true}
                            rotationAngle={45}
                            drawSliceText={true}
                            usePercentValues={false}
                            styledCenterText={{text: 'Pie center text!', color: processColor('pink'), size: 20}}
                            centerTextRadiusPercent={100}
                            holeRadius={40}
                            holeColor={processColor('#f0f0f0')}
                            transparentCircleRadius={45}
                            transparentCircleColor={processColor('#f0f0f088')}
                            maxAngle={350}
                            onSelect={this.handleSelect.bind(this)}
                            onChange={(event) => console.log(event.nativeEvent)}
                            />

                        <AwesomeAlert
                            show={showAlert}
                            showProgress={false}
                            title= {this.state.selectedEntry}
                            message="You are sure confirm pay for this jackpot!"
                            closeOnTouchOutside={true}
                            closeOnHardwareBackPress={false}
                            showCancelButton={true}
                            showConfirmButton={true}
                            cancelText="No, cancel"
                            confirmText="Yes, Pay"
                            confirmButtonColor="#009933"
                            cancelButtonColor="#ff3300"
                            onCancelPressed={() => {
                                        this.hideAlert();
                            }}
                            onConfirmPressed={() => {
                                            this.hideAlert();
                            }}
                            />
                        <StatusBar />

                    </View>
                </SafeAreaView>
                                            );
                        }
                    }


                    const styles = StyleSheet.create({
                        container: {
                            flex: 1,
                        },
                        chart: {
                            flex: 1
                        },
                        listContainer: {
                            paddingTop: 22
                        },
                        button: {
                            padding: 8,
                            alignItems: 'center',
                            borderRadius: 8,
                            margin: 8,
                            backgroundColor: '#222222',
                            borderWidth: 1,
                        }
                    });

                    export default withUser(withToast(withLoader(withRegisterUser(Home))));

                  /*  export default PieChartScreen; */
