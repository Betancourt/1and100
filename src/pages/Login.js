import React, {Component} from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {Content, Label, Grid, Row, Col, Button, Text} from 'native-base';
import {Actions} from 'react-native-router-flux';
import withToast from 'withToast';
import withLoader from 'withLoader';
import withUser from 'withUser';
import {storeUser, PROVIDER, FACEBOOK, SOCIAL_AUTH_CONFIG} from 'global';
import AppImages from 'AppImages';
import LoginFieldForm from '../components/LoginFieldForm';
import LoginStyle from 'LoginStyle';
import LayoutStyle from 'LayoutStyle';
import API from 'AppUtils';
import {facebook} from "react-native-simple-auth";

class Login extends Component {

    constructor(props) {
        super(props);
    }

    _appLogin = (payload) => {
        const {toast, setUser} = this.props;
        const {id_user, customer_name, customer_lastname, customer_email, nickname} = payload;
        const userData = {id_user, customer_name, customer_lastname, customer_email, nickname};
        storeUser(userData);
        setUser(userData);
        toast({text: 'Login success', type: 'success'});
        Actions.main();
    }

    _doLogin = async (loginData) => {
        const {loader, toast} = this.props;
        loader(true);
        loginData = Object.assign(loginData, {type: 0});
        await API.login(loginData).then((res) => {
            const {error, message, data} = res;
            loader(false);
            if (error) {
                toast({text: message, type: 'danger'});
            } else {
                const {response} = data;
                this._appLogin(response[0]);
            }
        });
    }

    _doFacebookLogin = async () => {
        const {loader, toast} = this.props;
        loader(true);
        await facebook(SOCIAL_AUTH_CONFIG.facebook)
            .then((data) => {
                const {credentials: {access_token: access_token}, user: {id}} = data
                if (data != null) {
                    let loginUserData = {
                        type: 1,
                        token: access_token,
                        fb_id: id
                    };
                    loginUserData = Object.assign(loginUserData, {type: 1});
                    API.login(loginUserData).then((res) => {
                        const {error, message, data} = res;
                        loader(false);
                        if (error) {
                            toast({text: message, type: 'danger'});
                        } else {
                            const {response} = data;
                            this._appLogin(response[0]);
                        }
                    });
                }
            });
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Content style={LayoutStyle.rootContainer} scrollEnabled={false}>
                    <Grid style={LayoutStyle.mainGrid}>
                        <Row style={LoginStyle.gridRow}>
                            <Col style={LoginStyle.gridCol}>
                                <Row style={LoginStyle.iconRow}>
                                    <Col>
                                        <Text style={LoginStyle.titleTxt}>
                                            Login
                                        </Text>
                                    </Col>
                                </Row>
                                <Grid>
                                    <Row style={LoginStyle.footerIconRow}>
                                        <TouchableOpacity style={LoginStyle.footerIconRow}
                                                          onPress={() => this._doFacebookLogin(this)}>
                                            <Col style={LoginStyle.footerIconColFB}>
                                                <Image style={LoginStyle.socialImages} source={AppImages.iconFacebook}/>
                                            </Col>
                                        </TouchableOpacity>
                                    </Row>
                                </Grid>
                                <Row style={LoginStyle.loginWithRow}>
                                    <Col>
                                        <Text style={LoginStyle.loginWithTxt}>
                                            or Sign In with:
                                        </Text>
                                    </Col>
                                </Row>
                                <LoginFieldForm {...this.props} onFacebookLogin={this._doFacebookLogin.bind(this)}
                                                onSubmit={this._doLogin.bind(this)}/>
                            </Col>
                        </Row>
                    </Grid>
                    <Row style={LoginStyle.signUpRow}>
                        <Row>
                            <Col style={LoginStyle.signUpTextCol}>
                                <Text style={LoginStyle.signUpBtnLabel}>Don't have an account ?
                                    <Text style={LoginStyle.signUpBtnTxt} onPress={() => Actions.register()}>
                                        &nbsp; Sign Up
                                    </Text>
                                </Text>
                            </Col>
                        </Row>
                    </Row>
                </Content>
            </View>
        );
    }
}

export default withUser(withToast(withLoader(Login)));
