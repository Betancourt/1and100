import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Menu from 'Menu';
import StyleConfig from 'StyleConfig';
import NavBar from './components/NavBar';

export default () => (
    <Router>
        <Scene key="root">
            <Scene type="replace" key="login" component={Login} initial panHandlers={null} hideNavBar={true}  navBar={() => <NavBar back="register"/>} />
            <Scene type="replace" key="register" component={Register}  panHandlers={null} hideNavBar={true} navBar={() => <NavBar back="login"/>}/>
            <Scene type="replace" key="home" component={Home} panHandlers={null} />
            <Scene type="replace" key="main" panHandlers={null} navBar={() => <NavBar />} >
                <Scene type="replace" key="drawer" panHandlers={null} drawer contentComponent={Menu}
                       drawerOpenRoute="DrawerOpen"
                       drawerToggleRoute="DrawerToggle"
                       drawerPosition="right"
                       drawerWidth={StyleConfig.drawerWidth}
                       hideNavBar={true}>
                        <Scene type="replace" panHandlers={null} key="home" component={Home} hideNavBar={true} headerLogo="pin" />
                </Scene>
            </Scene>
        </Scene>
    </Router>
);
